import 'vue-material/dist/vue-material.css' // get vue-material styles

import FastClick from 'fastclick';
import Vue from 'vue'; // get vue
import VueMaterial from 'vue-material'; // get UI library Vue Material
import axios from 'axios'; // get http
import VueAnalytics from 'vue-analytics'

import App from './app.vue'; // get root module
import router from './router';

FastClick.attach(document.body); // init fastclick
Vue.use(VueMaterial);
Vue.use(VueAnalytics, {
  id: 'UA-102442879-2',
  router,
})

const data = {
  options: {
    api: 'http://qldcamping.curiosat.com/get',
    id: '',
    interstate: false,
    latlng: '-27.4661,153.024',
    multiday: [],
    overnight: []
  }
};


// init
const app = new Vue({
  router,
  data: data,
  render: h => h(App),
})

VueAnalytics.onScriptLoaded().then(() => {
  app.$mount('#app');
})
