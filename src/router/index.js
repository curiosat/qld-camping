import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from 'axios';

import Home from '../views/home.vue';
import BestSpots from '../views/best-spots.vue';
import MapView from '../views/mapview.vue';

Vue.use(VueRouter);

const routes = [
  { path: '/', component: Home },
  { path: '/places/:id', component: BestSpots, props: (route) => ({
    route: route
  }) },
  { path: '/explore', component: MapView, props: (route) => ({
    latlng: route.query.latlng,
    is: route.query.is
  }) },
];

const router = new VueRouter({
  routes,
  mode: 'history'
});

export default router;
