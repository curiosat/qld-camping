# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
[1.0.1] - Styling updates and bugfixes
[1.0.0] - First iteration of the online suggestion tool
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
To start developing on this project, first do `npm install` to get all dependencies for the project. Once all dependencies have installed, you can run the project in development mode with `npm run dev`.
 webpack-dev-server -g`
* Dependencies
If you run into any problems, try installing webpack-dev-server globally by `npm install
* Notes about tools
This projects uses Material Design components of the [Vue-Material Library](http://vuematerial.io/#/getting-started).
* Deployment instructions
To build for production, do `npm run build`.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
